/*
 * $Header: /opt/cvsroot/Fred/test/com/cmdev/fred/struts/action/TestFredRenderer.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2002.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.fred.struts.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;

import com.cmdev.fred.struts.form.FredRenderable;

import junit.framework.TestCase;

/**
 * JUnit TestCase for FredRenderer
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class TestFredRenderer extends TestCase {

	/**
	 * Stub implementation of URIResolver for testing
	 * 
	 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
	 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
	 */
	private static class StubURIResolver implements URIResolver {

		/* (non-Javadoc)
		 * @see javax.xml.transform.URIResolver#resolve(java.lang.String, java.lang.String)
		 */
		public Source resolve(String arg0, String arg1)
			throws TransformerException {
			return null;
		}

	}
	private OutputStream stream;
	private FredRenderable renderable;
	private FredRenderer renderer;

	/**
	 * Constructor for FredRendererTest.
	 * @param arg0
	 */
	public TestFredRenderer(String arg0) {
		super(arg0);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		stream = new ByteArrayOutputStream();
		renderer = new FredRenderer(new StubURIResolver(), stream);
	}

	public void testRender() throws IOException {
		renderable = new MockFredRenderable();
		renderer.render(renderable, "/missing.xsl");
		stream.flush();
		System.out.println(stream.toString());
		assertTrue(
			"Misrendered, wrong length",
			stream.toString().length() > 100);
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		stream.close();
		renderer = null;
		super.tearDown();
	}

}

/*
 * $Header: /opt/cvsroot/Fred/test/com/cmdev/fred/struts/action/TestActionFormElement.java,v 1.1 2004/01/01 23:31:36 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2004.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.struts.action;

import org.apache.struts.action.ActionForm;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import junit.framework.TestCase;

/**
 * 
 * 
 * @author     <a href="mailto:sen@cookiehome.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $
 */
public class TestActionFormElement extends TestCase {
	
	DocumentFactory docFactory = DocumentFactory.getInstance();
	
	public void setUp() {

	}

	public void testCreateClassElement() {
		ActionForm aForm = new MockActionForm();
		ActionFormElement afe = new ActionFormElement(aForm);
		Document doc = docFactory.createDocument();
		Element root = docFactory.createElement("fredActionForm");
		doc.setRootElement(root);
		
		Element e = afe.createClassElement(root);
		assertNotNull("Null element returned", e);
		assertEquals("Wrong value for element name", "MockActionForm", e.getName());		
		assertEquals("Wrong path for element", "/fredActionForm/MockActionForm", e.getPath());

		afe = new ActionFormElement(null);
		e = afe.createClassElement(root);
		assertNotNull("Null element returned", e);
		assertEquals("Wrong value for element name", "FredNullForm", e.getName());		
		assertEquals("Wrong path for element", "/fredActionForm/FredNullForm", e.getPath());
	}
	
	public void testPropertyElements() {
		ActionForm aForm = new MockActionForm();
		ActionFormElement afe = new ActionFormElement(aForm);
		Document doc = docFactory.createDocument();
		Element root = docFactory.createElement("fredActionForm");
		doc.setRootElement(root);
		
		afe.propertyElements(root);
		
		System.out.println(root.asXML());
	}

}

/*
 */

package com.cmdev.fred.struts.action;

import org.apache.struts.action.ActionError;
import org.dom4j.Attribute;
import org.dom4j.Element;

import junit.framework.TestCase;

/**
 * TestCase for the FredStrutsRenderer
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class TestFredStrutsRenderer extends TestCase {

	private Object token;
	private FredStrutsRenderer myFredRenderer;
	/**
	 * Constructor for TestFredStrutsRenderer.
	 * @param arg0
	 */
	public TestFredStrutsRenderer(String b) {
		super(b);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		myFredRenderer = new FredStrutsRenderer();
	}

	public void testRenderActionError() {
		ActionError err = new ActionError("mock.error", "Mock Error 1", "Mock Error 2");
		MockFredRenderable renderable = new MockFredRenderable();
		Element e = myFredRenderer.render(err, "Errors Occurred");
		Attribute attribute = e.attribute("name");
		assertEquals("Wrong attribute rendering for ActionError", "mock.error", attribute.getValue());

		assertEquals("Wrong element count for ActionError", 2, e.elements("value").size());
		Element value = (Element) e.elements("value").get(0);
		assertEquals("Wrong element rendering for ActionError", "Mock Error 1", value.getText());
		value = (Element) e.elements("value").get(1);
		assertEquals("Wrong element rendering for ActionError", "Mock Error 2", value.getText());
		
		Element message = e.element("message");
		assertNotNull("No message", message);
		assertEquals("Wrong text for message", "Errors Occurred", message.getText());
	}
	
	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		myFredRenderer = null;
	}

}

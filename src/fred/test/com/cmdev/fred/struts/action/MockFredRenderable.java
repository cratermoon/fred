/*
 */

package com.cmdev.fred.struts.action;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import com.cmdev.fred.struts.form.FredRenderable;

/**
 * A MockObject for testing clients of FredRenderable
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
/**
 * Simple test mock object renderable
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
class MockFredRenderable implements FredRenderable {

	private DocumentFactory docFactory = DocumentFactory.getInstance();
	/**
	 * 
	 */
	public MockFredRenderable() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.cmdev.fred.struts.form.FredRenderable#renderToDom(org.w3c.dom.Document)
	 */
	public void renderToDom(Document doc) {
		Element fred = docFactory.createElement("fred");
		fred.addText("Hello, World");
		doc.getRootElement().add(fred);
	}

}

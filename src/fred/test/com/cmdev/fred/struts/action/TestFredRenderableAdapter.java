/*
 * $Header: /opt/cvsroot/Fred/test/com/cmdev/fred/struts/action/TestFredRenderableAdapter.java,v 1.2 2004/01/01 23:32:26 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.struts.action;

import org.apache.struts.action.ActionForm;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Node;

import com.cmdev.fred.struts.form.FredRenderableAdapter;

import junit.framework.TestCase;

/**
 * 
 * 
 * @author     <a href="mailto:sen@cookiehome.com">Steven E. Newton</a>
 * @version    $Revision: 1.2 $
 */
public class TestFredRenderableAdapter extends TestCase {

	public void testRenderToDom() {
		ActionForm testForm = new MockActionForm();
		FredRenderableAdapter testAdapter = new FredRenderableAdapter(testForm);
		DocumentFactory docFactory = DocumentFactory.getInstance();
		Document doc = docFactory.createDocument();
		testAdapter.renderToDom(doc);
		Node n = doc.selectSingleNode("//propertyOne");
		assertNotNull("node propertyOne doesn't exist", n);
		n = doc.selectSingleNode("//class");
		assertNull("node class should not exist", n);
	}

}

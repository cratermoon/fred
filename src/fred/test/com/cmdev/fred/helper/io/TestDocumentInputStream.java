/*
 */

package com.cmdev.fred.helper.io;

import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import junit.framework.TestCase;

/**
 * TestCase for DocumentInputStream.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:21 $
 */
public class TestDocumentInputStream extends TestCase {

	private Document testDoc;
	private DocumentInputStream dis;
	/**
	 * Constructor for TestDocumentInputStream.
	 * @param arg0
	 */
	public TestDocumentInputStream(String b) {
		super(b);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		testDoc = DocumentHelper.createDocument();
		dis = new DocumentInputStream(testDoc);
	}

	/**
	 * Test for int read(byte[])
	 * Just tests streaming the empty Document.
	 */
	public void testReadbyteArray() throws IOException {
		byte[] someBytes = new byte[4096];
		int result = dis.read(someBytes);
		assertEquals("Read too many bytes", 39, result);
		System.out.println(new String(someBytes).trim());
		assertEquals("Incorrectly read XML", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", new String(someBytes).trim());
	}
	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		dis.close();
	}


}

/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/model/Menu.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.javashop.model;

import java.util.HashMap;


/**
 *  The java shop's menu. Maintains available selections of beverages,
 *  sizes, and condiments.
 *
 * @author     <a href="mailto:snewton@standard.com">Steven E. Newton</a>
 * @created    July 29, 2002
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */

public class Menu {

    private HashMap availableBeverages;
    private CondimentCollection availableCondiments;
    private HashMap availableSizes;


    /**
     *  Constructor for the Menu object
     */
    public Menu() {
        initializeBeverages();
        initializeCondiments();
        initializeBreads();
    }


    /**
     *  Constructor for the Menu object
     */
    public Menu(HashMap beverages, HashMap breads, CondimentCollection condiments) {
        this.availableBeverages = beverages;
        this.availableSizes = breads;
        this.availableCondiments = condiments;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Returned Value
     */
    public HashMap sizes() {
        return availableSizes;
    }


    /**
     *  Description of the Method
     *
     * @return    Description of the Returned Value
     */
    public HashMap beverages() {
        return availableBeverages;
    }


    /**
     *  Description of the Method
     *
     * @return    Description of the Returned Value
     */
    public CondimentCollection condiments() {
        return availableCondiments;
    }

    public Beverage getBeverage(String aBeverageType) {
        return (Beverage)availableBeverages.get(aBeverageType);
    }

    public Condiment getCondiment(String aLabel) {
        return availableCondiments.getCondiment(aLabel);
    }

    public BeverageSize getBread(String aBreadType) {
        return (BeverageSize)availableSizes.get(aBreadType);
    }

    private void initializeBeverages() {
        availableBeverages = new HashMap();
        Beverage aBeverage;
        aBeverage = new Beverage();
        aBeverage.setLabel("Latte");
        aBeverage.setPrice(4.5f);
        availableBeverages.put(aBeverage.getLabel(), aBeverage);

        aBeverage = new Beverage();
        aBeverage.setLabel("Cappucino");
        aBeverage.setPrice(3.5f);
        availableBeverages.put(aBeverage.getLabel(), aBeverage);

        aBeverage = new Beverage();
        aBeverage.setLabel("Mocha");
        aBeverage.setPrice(4.0f);
        availableBeverages.put(aBeverage.getLabel(), aBeverage);
    }


    private void initializeCondiments() {
        availableCondiments = new CondimentCollection();

        Condiment aCondiment = new Condiment();
        aCondiment.setLabel("Whipped Cream");
        aCondiment.setPrice(0.0f);
        availableCondiments.addCondiment(aCondiment);

        aCondiment = new Condiment();
        aCondiment.setLabel("Soy Milk");
        aCondiment.setPrice(0.0f);
        availableCondiments.addCondiment(aCondiment);

        aCondiment = new Condiment();
        aCondiment.setLabel("Extra Shot");
        aCondiment.setPrice(0.50f);
        availableCondiments.addCondiment(aCondiment);
    }


    private void initializeBreads() {
        availableSizes = new HashMap();

        BeverageSize aBread = new BeverageSize();
        aBread.setLabel("Tall");
        availableSizes.put(aBread.getLabel(), aBread);

        aBread = new BeverageSize();
        aBread.setLabel("Grande");
        availableSizes.put(aBread.getLabel(), aBread);

        aBread = new BeverageSize();
        aBread.setLabel("Vente");
        availableSizes.put(aBread.getLabel(), aBread);

        aBread = new BeverageSize();
        aBread.setLabel("OMiGod");
        availableSizes.put(aBread.getLabel(), aBread);
    }
}

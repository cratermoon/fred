package com.cmdev.javashop.model;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author <a href="mailto:snewton@standard.com">Steven E. Newton</a>
 * @version $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */

public abstract class MenuItem  {
    private String label;
    private float price;

    public MenuItem() {}

    /**
     *  Sets the Label attribute of this MenuItem
     *
     * @param  label  The new Label value
     */
    public void setLabel(String label) {
        this.label = label;
    }


    /**
     *  Sets the Price attribute of this MenuItem
     *
     * @param  price  The new Price value
     */
    public void setPrice(float price) {
        this.price = price;
    }


    /**
     *  Gets the Label attribute of this MenuItem
     *
     * @return    The Label value
     */
    public String getLabel() {
        return label;
    }


    /**
     *  Gets the Price attribute of tthis MenuItem
     *
     * @return    The Price value
     */
    public float getPrice() {
        return price;
    }

    public String toString() {
        return getLabel();
    }
}
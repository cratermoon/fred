/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/model/BeverageOrder.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.javashop.model;

import java.util.Iterator;

/**
 *
 * @author <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */

public class BeverageOrder {
    private Beverage beverageType = new Beverage();
    private BeverageSize beverageSize;
    private CondimentCollection condiments;
    private float totalPrice;

    public BeverageSize beverageSize() {
        return beverageSize;
    }
    public void setBeverageSize(BeverageSize beverageSize) {
        this.beverageSize = beverageSize;
    }
    public String getBeverageSizeLabel() {
        if (beverageSize != null) {
            return beverageSize.getLabel();
        } else {
            return null;
        }
    }

    public void setCondiments(CondimentCollection condiments) {
        this.condiments = condiments;
    }

    public CondimentCollection getCondiments() {
        return condiments;
    }

    public String getCondimentLabels() {
        StringBuffer cLabels = new StringBuffer();
        Iterator condimIterator = this.condimentsIterator();
        cLabels.append(this.condimentsCount());
        while (condimIterator.hasNext()) {
            cLabels.append(", ");
            cLabels.append(condimIterator.next());
        }
        return cLabels.toString();
    }

    public Iterator condimentsIterator() {
        if (condiments == null) {
            condiments = new CondimentCollection();
        }
        return condiments.iterator();
    }

    public int condimentsCount() {
        if (condiments != null) {
            return condiments.size();
        } else {
            return 0;
        }
    }

    public String[] condimentLabels() {
        Iterator condimentIterator = condimentsIterator();
        String[] condimentLabels = new String[condimentsCount()];
        if (condimentIterator != null) {
            int index = 0;
            while (condimentIterator.hasNext()) {
                Condiment thisCondiment = (Condiment) condimentIterator.next();
                condimentLabels[index++] = thisCondiment.getLabel();
            }
        }
        return condimentLabels;
    }

    public Beverage getBeverageType() {
        return beverageType;
    }

    public String getBeverageTypeLabel() {
        if (beverageType != null) {
            return beverageType.getLabel();
        } else {
            return null;
        }
    }

    public void setBeverageType(Beverage beverageType) {
        this.beverageType = beverageType;
    }
    public float getTotalPrice() {
        totalPrice = beverageType.getPrice();
        Iterator condimentIterator = condimentsIterator();
        if (condimentIterator != null) {
            while (condimentIterator.hasNext()) {
                Condiment thisCondiment = (Condiment) condimentIterator.next();
                totalPrice += thisCondiment.getPrice();
            }
        }
        return totalPrice;
    }
}

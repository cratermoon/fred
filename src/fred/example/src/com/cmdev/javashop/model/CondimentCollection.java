package com.cmdev.javashop.model;

import java.util.HashMap;
import java.util.Iterator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author <a href="mailto:snewton@standard.com">Steven E. Newton</a>
 * @version $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */

public class CondimentCollection {
    private HashMap theCondiments = new HashMap();

    public void addCondiment(Condiment aCondiment) {
        theCondiments.put(aCondiment.getLabel(), aCondiment);
    }

    public Condiment getCondiment(String aLabel) {
        return (Condiment)theCondiments.get(aLabel);
    }

    public Condiment removeCondiment(Condiment aCondiment) {
        return (Condiment)theCondiments.remove(aCondiment.getLabel());
    }

    public Iterator iterator() {
        return theCondiments.values().iterator();
    }

    public int size() {
        return theCondiments.size();
    }
}
/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/BeverageOrderForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.javashop.struts.form;

import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.dom4j.*;

import com.cmdev.javashop.model.BeverageOrder;
import com.cmdev.fred.struts.form.FredActionForm;

/**
 * Handle choosing the beverage
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class BeverageOrderForm extends FredActionForm {

	private BeverageOrder theOrder;
	private String[] condimentSelections;
	private static int orderNumber;
	// --------------------------------------------------------- Instance Variables
	private float totalPrice = 0;
	private String beverageType = "No Order Yet";
	private String breadType = "Not Selected";
	private DocumentFactory docFactory = DocumentFactory.getInstance();
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

	// --------------------------------------------------------- Methods

	public BeverageOrderForm() {
		clear();
	}
	/** 
	 * Method validate
	 * @param ActionMapping mapping
	 * @param HttpServletRequest request
	 * @return ActionErrors
	 */
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		String path = mapping.getPath();
		if (path.equals("/MakeBeverage") && beverageType.equals("No Order Yet")) {
			ActionErrors errs = new ActionErrors();
			errs.add("beverageType", new ActionError("beverage.choice.missing", "You must choose a beverage"));
			return errs;
		} else {
			return super.validate(mapping, request);
		}
	}

	/* (non-Javadoc)
	 * @see com.cmdev.fred.struts.form.FredActionForm#renderToDom(org.w3c.dom.Document)
	 */
	public void renderToDom(Document doc) {
		Element root = docFactory.createElement("javaShop");
		root.setName("javaShop");
		doc.getRootElement().add(root);

		Element menu = addMenu(root);
		Element order = addOrder(menu);
		addBreadSelection(order);
		addBeverageSelection(order);
		addCondiments(order);
		addTotalPrice(order);
		System.out.println(doc.asXML());
	}

	/**
	 * @param order
	 */
	private void addTotalPrice(Element order) {
		Element tp = docFactory.createElement("totalPrice");
		tp.addText(currencyFormat.format(getTotalPrice()));
		order.add(tp);
	}

	/**
	 * @return
	 */
	private float getTotalPrice() {
		return totalPrice;
	}

	private void addBeverageSelection(Element order) {
		Element e;
		e = docFactory.createElement("beverageType");
		e.addText(getBeverageType());
		order.add(e);
	}
	private void addBreadSelection(Element order) {
		Element e;
		e = docFactory.createElement("beverageSize");
		e.addText(getBeverageSize());
		order.add(e);
	}
	private Element addOrder(Element menu) {
		Element order = docFactory.createElement("beverageOrder");
		menu.add(order);
		return order;
	}
	private Element addMenu(Element root) {
		Element menu;
		menu = docFactory.createElement("mainMenu");
		root.add(menu);	
		Element onElement = docFactory.createElement("orderNumber");
		onElement.addText(String.valueOf(orderNumber));
		menu.add(onElement);
		return menu;
	}

	/**
	 * @param order
	 */
	private void addCondiments(Element order) {
		Element condiments = docFactory.createElement("condiments");
		order.add(condiments);
		for (int i = 0; i < condimentSelections.length; i++) {
			Element c = docFactory.createElement("condiment");
			c.addText(condimentSelections[i]);
			condiments.add(c);
		}
		
	}

	/**
	 * @return
	 */
	public String getBeverageSize() {
		return breadType;
	}

	/**
	 * @return
	 */
	public String getBeverageType() {
		return beverageType;
	}

	/**
	 * @param string
	 */
	public void setBeverageSize(String string) {
		breadType = string;
	}

	/**
	 * @param string
	 */
	public void setBeverageType(String string) {
		beverageType = string;
	}

	/**
	 * @param string
	 */
	public void setTotalPrice(float f) {
		totalPrice = f;
	}
	
	public void setCondimentSelections(String[] s) {
		condimentSelections = s;
	}

	public String[] getCondimentSelections() {
		return condimentSelections;
	}

	public void clear() {
		totalPrice = 0.0f;
		beverageType = "No Order Yet";
		breadType = "Not Selected";
		condimentSelections = new String[] {};
		orderNumber++;
	}

}

// Created by Xslt generator for Eclipse.
// XSL :  not found (java.io.FileNotFoundException:  (The system cannot find the path specified))
// Default XSL used : easystruts.jar$org.easystruts.xslgen.JavaClass.xsl

package com.cmdev.javashop.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.cmdev.javashop.struts.form.ChooseSizeForm;
import com.cmdev.fred.struts.action.FredAction;
import com.cmdev.javashop.model.Menu;

/** 
 * BreadMenuAction.java created by EasyStruts - XsltGen.
 * http://easystruts.sf.net
 * created on 06-04-2003
 * 
 * XDoclet definition:
 * @struts:action path="/BreadMenu" name="mainMenuForm" validate="true"
 * @struts:action-forward name="/BreadMenu.xsl" path="/BreadMenu.xsl"
 */
public class ChooseSizeAction extends FredAction {

	// --------------------------------------------------------- Instance Variables
	private Menu myMenu = new Menu();

	// --------------------------------------------------------- Methods

	/** 
	 * Method execute
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws Exception
	 */
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		ChooseSizeForm myForm = (ChooseSizeForm) form;
		myForm.setAvailableSizes(myMenu.sizes());
		return mapping.findForward("OK");
	}

}

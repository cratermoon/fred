/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/ChooseBeverageForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.javashop.struts.form;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


import com.cmdev.fred.struts.form.FredActionForm;
import com.cmdev.javashop.model.Beverage;

public class ChooseBeverageForm extends FredActionForm {

	// --------------------------------------------------------- Instance Variables

	private Map beverageMenuListing;
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

	/**
	 * 
	 */
	public ChooseBeverageForm() {
		super();
		beverageMenuListing = new HashMap();
	}

	// --------------------------------------------------------- Methods

	/** 
	 * Returns the BeverageMenuListing.
	 * @return String
	 */
	public Map getBeverageMenuListing() {
		return beverageMenuListing;
	}

	public void setBeverageMenuListing(Map beverages) {
		beverageMenuListing.putAll(beverages);
	}

	public void renderToDom(Document doc) {
		Element shop = DocumentHelper.createElement("javaShop");
		Element e = DocumentHelper.createElement("beverageMenu");
		shop.add(e);
		Element root = doc.getRootElement();
		root.add(shop);
		
		Iterator iter = beverageMenuIterator();
		while (iter.hasNext()) {
			Beverage s = (Beverage) iter.next();
			e.add(createBeverageElement(s));
		}
	}

	private Iterator beverageMenuIterator() {
		return beverageMenuListing.values().iterator();
	}

	/**
	 * @param s
	 * @return
	 */
	private Element createBeverageElement(Beverage s) {
		Element e = DocumentHelper.createElement("beverage");
		Element label = DocumentHelper.createElement("label");
		label.addText(s.getLabel());
		e.add(label);
		Element price = DocumentHelper.createElement("price");
		price.addText(currencyFormat.format(s.getPrice()));
		e.add(price);
		return e;
	}
}

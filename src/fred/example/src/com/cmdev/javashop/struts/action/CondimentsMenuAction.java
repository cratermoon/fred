// Created by Xslt generator for Eclipse.
// XSL :  not found (java.io.FileNotFoundException:  (The system cannot find the path specified))
// Default XSL used : easystruts.jar$org.easystruts.xslgen.JavaClass.xsl

package com.cmdev.javashop.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.cmdev.javashop.struts.form.ChooseCondimentsForm;
import com.cmdev.fred.struts.action.FredAction;

/** 
 * CondimentsMenuAction.java created by EasyStruts - XsltGen.
 * http://easystruts.sf.net
 * created on 06-04-2003
 * 
 * XDoclet definition:
 * @struts:action path="/CondimentsMenu" name="mainMenuForm" validate="true"
 * @struts:action-forward name="/CondimentsMenu.xsl" path="/CondimentsMenu.xsl"
 */
public class CondimentsMenuAction extends FredAction {

	// --------------------------------------------------------- Instance Variables

	// --------------------------------------------------------- Methods

	/** 
	 * Method execute
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws Exception
	 */
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		ChooseCondimentsForm myForm = (ChooseCondimentsForm) form;
		return mapping.findForward("OK");
	}

}

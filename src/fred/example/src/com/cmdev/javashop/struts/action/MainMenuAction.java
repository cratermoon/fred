/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/action/MainMenuAction.java,v 1.1 2003/12/30 14:43:21 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.javashop.struts.action;

import java.text.NumberFormat;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.cmdev.javashop.struts.form.BeverageOrderForm;
import com.cmdev.fred.struts.action.FredAction;
import com.cmdev.javashop.model.Condiment;
import com.cmdev.javashop.model.Menu;
import com.cmdev.javashop.model.Beverage;

/**
 *  
 */
public class MainMenuAction extends FredAction {

	// --------------------------------------------------------- Instance Variables

	private Menu myMenu = new Menu();
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	// --------------------------------------------------------- Methods

	/** 
	 * Method execute
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws Exception
	 */
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		BeverageOrderForm mainMenuForm = (BeverageOrderForm) form;
		
		handleActionErrors(request);
		if (request.getParameter("reset") != null
			&& mainMenuForm != null) { 
			mainMenuForm.clear(); 
		}
		updatePrice(mainMenuForm);
		return mapping.findForward("OK");
	}

	private void handleActionErrors(HttpServletRequest request) {
		ActionErrors errs = (ActionErrors) request.getAttribute(Globals.ERROR_KEY);
		if (errs != null) {
			Iterator iterator = errs.get("beverageType");
			while (iterator.hasNext()) {
				ActionError element = (ActionError) iterator.next();
				System.out.println(element.getKey());
			
			}
		}
	}

	private void updatePrice(BeverageOrderForm mainMenuForm) {
		Beverage s = myMenu.getBeverage(mainMenuForm.getBeverageType());
		float totalPrice = 	priceCondiments(mainMenuForm);
		if (s != null) {
			totalPrice += s.getPrice();
		}
		mainMenuForm.setTotalPrice(totalPrice);
	}

	/**
	 * @param mainMenuForm
	 */
	private float priceCondiments(BeverageOrderForm mainMenuForm) {
		float condimentCost = 0.0f;
		String[] cs = mainMenuForm.getCondimentSelections();
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				Condiment c = myMenu.getCondiment(cs[i]);
				if (c != null) {
					condimentCost += c.getPrice();
					
				}
			}
		}
		return condimentCost;		
	}

}

/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/ChooseSizeForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.javashop.struts.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.cmdev.fred.struts.form.FredActionForm;
import com.cmdev.javashop.model.BeverageSize;

/**
 * Handle choosing the size of the selected beverage
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class ChooseSizeForm extends FredActionForm {

	Map availableSizes;
	/**
	 * 
	 */
	public ChooseSizeForm() {
		super();
		availableSizes = new HashMap();
	}

	public void setAvailableSizes(Map m) {
		availableSizes.putAll(m);
	}

	public Map getAvailableSizes() {
		return availableSizes;
	}
	/* (non-Javadoc)
	 * @see com.standard.fred.struts.form.SFGRenderable#renderToDom(org.dom4j.Document)
	 */
	public void renderToDom(Document doc) {
		Element shop = DocumentHelper.createElement("javaShop");
		Element e = DocumentHelper.createElement("beverageSizeMenu");
		shop.add(e);
		doc.getRootElement().add(shop);

		Iterator iter = availableSizesIterator();
		while (iter.hasNext()) {
			BeverageSize b = (BeverageSize) iter.next();
			e.add(createBeverageSizeElement(b));
		}

	}

	/**
	 * @param s
	 * @return
	 */
	private Element createBeverageSizeElement(BeverageSize s) {
		Element b = DocumentHelper.createElement("beverageSize");
		Element l = DocumentHelper.createElement("label");
		b.add(l);
		l.addText(s.getLabel());
		return b;
	}

	/**
	 * @return
	 */
	private Iterator availableSizesIterator() {
		return availableSizes.values().iterator();
	}

}

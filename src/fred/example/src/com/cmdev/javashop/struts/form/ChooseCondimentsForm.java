/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/ChooseCondimentsForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.javashop.struts.form;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.cmdev.fred.struts.form.FredActionForm;
import com.cmdev.javashop.model.Condiment;
import com.cmdev.javashop.model.CondimentCollection;
import com.cmdev.javashop.model.Menu;

/** 
 * ChooseCondimentsForm.java created by EasyStruts - XsltGen.
 * http://easystruts.sf.net
 */
public class ChooseCondimentsForm extends FredActionForm {

	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

	// --------------------------------------------------------- Instance Variables
	private Menu myMenu = new Menu();

	// --------------------------------------------------------- Methods

	public ChooseCondimentsForm() {
		super();
	}
	
	public List getAvailableCondiments() {
		CondimentCollection c = myMenu.condiments();
		ArrayList condiments = new ArrayList();
		for (Iterator iter = c.iterator(); iter.hasNext();) {
			condiments.add(iter.next());
			
		}
		return condiments;
	}
	/* (non-Javadoc)
	 * @see com.cmdev.fred.struts.form.FredActionForm#renderToDom(org.dom4j.Document)
	 */
	public void renderToDom(Document doc) {
		Element shop = DocumentHelper.createElement("javaShop");
		Element e = DocumentHelper.createElement("condimentsMenu");
		shop.add(e);
		doc.getRootElement().add(shop);
		
		addCondiments(e);
		System.out.println(doc.asXML());
	}

	/**
	 * @param e
	 */
	private void addCondiments(Element e) {
		
		for (Iterator iter = myMenu.condiments().iterator(); iter.hasNext();) {
			Condiment element = (Condiment) iter.next();
			addCondiment(e, element);
		}
	}

	/**
	 * @param e
	 * @param element
	 */
	private void addCondiment(Element e, Condiment element) {
			Element c = DocumentHelper.createElement("condiment");
			// TODO selected?
			Element label = DocumentHelper.createElement("label");
			label.addText(element.getLabel());
			c.add(label);
			Element price = DocumentHelper.createElement("price");
			price.addText(currencyFormat.format(element.getPrice()));
			c.add(price);
			e.add(c);
	}
}

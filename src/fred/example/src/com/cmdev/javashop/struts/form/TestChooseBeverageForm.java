/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/TestChooseBeverageForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.javashop.struts.form;

import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.cmdev.javashop.model.Menu;

import junit.framework.TestCase;

/**
 * Test case for ChooseBeverageForm
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class TestChooseBeverageForm extends TestCase {

	private ChooseBeverageForm csForm;
	private Menu menu = new Menu();
	/**
	 * Constructor for TestChooseBeverageForm.
	 * @param arg0
	 */
	public TestChooseBeverageForm(String b) {
		super(b);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		csForm = new ChooseBeverageForm();
		csForm.setBeverageMenuListing(menu.beverages());
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testRenderToDom() {
		Element root = DocumentHelper.createElement("fredForm");
		Document d = DocumentHelper.createDocument(root);
		csForm.renderToDom(d);
		System.out.println(d.asXML());
	}

	public void testGetBeverageMenuListing() {
		Map m = csForm.getBeverageMenuListing();
		assertNotNull("No beverages to choose", m);
	}

}

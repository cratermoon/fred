/*
 * $Header: /opt/cvsroot/Fred/example/src/com/cmdev/javashop/struts/form/TestBeverageOrderForm.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.javashop.struts.form;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import junit.framework.TestCase;

/**
 * Test case for BeverageOrderForm
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class TestBeverageOrderForm extends TestCase {

	private BeverageOrderForm testForm;
	/**
	 * Constructor for TestMainMenuForm.
	 * @param arg0
	 */
	public TestBeverageOrderForm(String arg0) {
		super(arg0);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		testForm = new BeverageOrderForm();
		testForm.setBeverageSize("Tall");
		testForm.setBeverageType("Latte");
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		testForm = null;
	}

	public void testRenderToDom() {
		Element root = DocumentHelper.createElement("fredForm");
		Document doc = DocumentHelper.createDocument(root);
		testForm.renderToDom(doc);
		System.out.println(doc.asXML());
	}

}

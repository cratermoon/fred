<?xml version="1.0" encoding="UTF-8"?>
<!-- Java Shop Confirmation
    $Header: /opt/cvsroot/Fred/example/webapp/MakeBeverage.xsl,v 1.1 2003/12/30 14:43:20 sen Exp $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="fredRenderable/javaShop/mainMenu"/>
    </xsl:template>
	<xsl:template match="mainMenu">
        <html>
        <head>
        <title>Java Shop Checkout</title>
        <link rel="stylesheet" href="javashop.css"/>
        <meta name="author" content="Steven E. Newton"/>
        </head>

        <body>
        <h1>Java Shop</h1>
        <p>Thank you for visiting the Crater Moon Java Shop.  Your order is being
        hand-crafted for you and will be ready at the pick-up window shortly.</p>
        <h2>Order number <xsl:value-of select="orderNumber"/></h2>
        <xsl:apply-templates select="beverageOrder"/>
        <hr/>
        <ul>
            <li><a href="shop.xml?reset=true">Order Another Beverage</a></li>
            <li><a href="index.html">Exit</a></li>
        </ul>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="beverageOrder">
        <ul>
        <li>Beverage selection: <xsl:value-of select="beverageType"/></li>
        <li>Beverage size selection: <xsl:value-of select="beverageSize"/></li>
        <li>Condiment selections:
          <xsl:apply-templates select="condiments"/>
        </li>
        <li>Total: <xsl:value-of select="totalPrice"/></li>
        </ul>
    </xsl:template>

    <xsl:template match="condiment">
        <ul>
        <li><xsl:value-of select="."/></li>
        </ul>
    </xsl:template>
</xsl:stylesheet>

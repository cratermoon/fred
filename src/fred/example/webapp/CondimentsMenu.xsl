<?xml version="1.0" encoding="UTF-8"?>
<!-- Java Shop Condiments Menu
    $Header: /opt/cvsroot/Fred/example/webapp/CondimentsMenu.xsl,v 1.1 2003/12/30 14:43:20 sen Exp $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="fredRenderable/javaShop/condimentsMenu"/>
    </xsl:template>
	<xsl:template match="condimentsMenu">
        <html>
        <head>
        <title>Choose Your Condiments</title>
        <link rel="stylesheet" href="javashop.css"/>
        <meta name="author" content="Steven E. Newton"/>
        </head>

        <body>
        <h1>Choose Your Condiments</h1>
        <h2>Order number <xsl:value-of select="orderNumber"/></h2>
            <form method="POST" action="shop.xml">
            <table>
            <tr><th>Selected</th><th>Condiment</th><th>Price</th></tr>
            <xsl:apply-templates select="condiment"/>
            </table>
            <input type="submit" value=" Continue "/>
            </form>
            <hr/>
        <a href="shop.xml">Main Menu</a>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="condiment">
        <tr>
            <td>
                <xsl:choose>
                    <xsl:when test="@checked">
                        <input type="checkbox" checked="yes" name="condimentSelections" value="{label}"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <input type="checkbox" name="condimentSelections" value="{label}"/>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td><xsl:value-of select="label"/></td>
            <td><xsl:value-of select="price"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>

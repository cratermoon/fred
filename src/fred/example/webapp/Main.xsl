<?xml version="1.0" encoding="UTF-8"?>
<!-- Java Shop Main
    $Header: /opt/cvsroot/Fred/example/webapp/Main.xsl,v 1.1 2003/12/30 14:43:20 sen Exp $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="fredRenderable/javaShop/mainMenu"/>
    </xsl:template>
    
	<xsl:template match="mainMenu">
        <html>
        <head>
        <title>Java Shop Ordering</title>
        <link rel="stylesheet" href="javashop.css"/>
        <meta name="author" content="Steven E. Newton"/>
        </head>

        <body>
        <h1>Java Shop</h1>
        <ul>
            <li><a href="BeverageMenu.xml">Choose Beverage</a></li>
            <li><a href="BeverageSizesMenu.xml">Choose Size</a></li>
            <li><a href="CondimentsMenu.xml">Choose Condiments</a></li>
            <li><a href="MakeBeverage.xml">Make Beverage</a></li>
        </ul>
        <h2>Order number <xsl:value-of select="orderNumber"/></h2>
	    <xsl:apply-templates select="/fredRenderable/actionError"/>
        <xsl:apply-templates select="beverageOrder"/>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="beverageOrder">
        <ul>
        <li>Beverage selection: <strong><xsl:value-of select="beverageType"/></strong></li>
        <li>Flavor selection: <strong><xsl:value-of select="flavors"/></strong></li>
        <li>Condiment selections:
          <xsl:apply-templates select="condiments"/>
        </li>
        <li>Total: <strong><xsl:value-of select="totalPrice"/></strong></li>
        </ul>
    </xsl:template>

    <xsl:template match="condiment">
        <ul>
        <li><strong><xsl:value-of select="."/></strong></li>
        </ul>
    </xsl:template>
    
    <xsl:template match="actionError">
        <p><em><xsl:value-of select="message"/></em></p>
    </xsl:template>
</xsl:stylesheet>

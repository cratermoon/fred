<?xml version="1.0" encoding="UTF-8"?>
<!-- Java Shop Error Page
    $Header: /opt/cvsroot/Fred/example/webapp/ErrorPage.xsl,v 1.1 2003/12/30 14:43:20 sen Exp $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/javaShop">
        <xsl:variable name="txnNumber" select="transactionNbr"/>
        <xsl:variable name="exceptionMessage" select="exception"/>
        <html>
        <head>
        <title>Error in Beverage Shop Ordering</title>
        <link rel="stylesheet" href="javashop.css"/>
        <meta name="author" content="Steven E. Newton"/>
        </head>

        <body>
        <h1>Error - Java Shop</h1>
        <h2>Order for customer number <xsl:value-of select="$txnNumber"/></h2>
        <strong><xsl:value-of select="$exceptionMessage"/></strong>
        </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

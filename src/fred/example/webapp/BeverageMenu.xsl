<?xml version="1.0" encoding="UTF-8"?>
<!-- Java Shop
    $Header: /opt/cvsroot/Fred/example/webapp/BeverageMenu.xsl,v 1.1 2003/12/30 14:43:20 sen Exp $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="fredRenderable/javaShop/beverageMenu"/>
    </xsl:template>
	<xsl:template match="beverageMenu">
        <html>
        <head>
        <title>Choose Your Beverage</title>
        <link rel="stylesheet" href="javashop.css"/>
        <meta name="author" content="Steven E. Newton"/>
        </head>

        <body>
        <h1>Choose Your Beverage</h1>
            <table>
            <tr><th>Beverage</th><th>Price</th></tr>
            <xsl:apply-templates select="beverage"/>
            </table>
            <hr/>
        <a href="shop.xml">Main Menu</a>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="beverage">
        <tr>
            <td>
               <form method="POST" action="shop.xml">
                <input type="submit" value=" x "/>
                <input type="hidden" name="beverageType" value="{label}"/>
               </form>
            </td>
            <td><xsl:value-of select="label"/></td>
            <td><xsl:value-of select="price"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>

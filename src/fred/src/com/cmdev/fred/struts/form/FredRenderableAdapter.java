/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/struts/form/FredRenderableAdapter.java,v 1.3 2004/01/01 23:31:07 sen Exp $
 * ====================================================================
 * Copyright (c) Steven E. Newton, 2003.  All rights reserved under
 * copyright laws of the United States and other countries.
 *
 * You may distribute under the terms of either the GNU General Public
 * License or the Artistic License, as specified in the README file.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package com.cmdev.fred.struts.form;

import org.apache.struts.action.ActionForm;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import com.cmdev.fred.logging.LogFacility;
import com.cmdev.fred.struts.action.ActionFormElement;

/**
 * Adds FredRenderable capabilities to any ActionForm, using a default
 * bean serialization technique.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.3 $ $Date: 2004/01/01 23:31:07 $
 */
public class FredRenderableAdapter implements FredRenderable {

	private DocumentFactory docFactory = DocumentFactory.getInstance();
	private ActionForm myForm;
	/**
	 * 
	 */
	public FredRenderableAdapter(ActionForm form) {
		super();
		myForm = form;
	}

	public void renderToDom(Document doc) {
		try {
			Element root = docFactory.createElement("fredActionForm");
			root.setName("fredActionForm");
			doc.setRootElement(root);
			ActionFormElement afe = new ActionFormElement(myForm);
			Element classElement = afe.createClassElement(root);
			afe.propertyElements(classElement);
		} catch (Exception e) {
			LogFacility.log(e);
		}
	}
}

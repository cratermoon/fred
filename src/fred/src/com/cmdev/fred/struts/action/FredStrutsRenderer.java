/*
 */

package com.cmdev.fred.struts.action;

import org.apache.struts.action.ActionError;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

/**
 * Generates XML documents from Struts miscellaneous helper classes.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:20 $
 */
public class FredStrutsRenderer {

	private DocumentFactory factory = DocumentFactory.getInstance();
	/**
	 * 
	 */
	public FredStrutsRenderer() {
		super();
	}

	/**
	 * @param err The ActionError to serialize as an Element
	 * @param message Any message associated with the ActionError
	 */
	public Element render(ActionError err, String message) {
		Element errorElt = factory.createElement("actionError");
		
		if (err == null) {
			return errorElt;
		}
		
		errorElt.addAttribute("name", err.getKey());
		if (message != null) {
			addMessageElement(errorElt, message);
		}
		Object[] values = err.getValues();
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				if (values[i] != null) {
					addValueElement(errorElt, values[i]);
				}
			}
		}
		return errorElt;
	}

	private void addValueElement(Element errorElt, Object value) {
		errorElt.add(factory.createElement("value").addText(value.toString()));
	}

	private void addMessageElement(Element errorElt, String message) {
		Element messageElement = factory.createElement("message");
		messageElement.addText(message); 
		errorElt.add(messageElement);
	}

}

/*
 */
package com.cmdev.fred.struts.action;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.RequestProcessor;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.MessageResources;
import org.dom4j.Element;

import com.cmdev.fred.logging.LogFacility;
import com.cmdev.fred.struts.form.FredRenderable;
import com.cmdev.fred.struts.form.FredRenderableAdapter;

/**
 * Replace the normal handling with XML/XSL transform
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.3 $
 */
public class FredRequestProcessor extends RequestProcessor {

	private FredXslPlugin xslPlugin;

	public FredRequestProcessor() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.RequestProcessor#init(org.apache.struts.action.ActionServlet, org.apache.struts.config.ModuleConfig)
	 */
	public void init(ActionServlet actionServlet, ModuleConfig config)
		throws ServletException {
		super.init(actionServlet, config);
		LogFacility.log("FredRequestProcessor initializing.");
		xslPlugin =
			(FredXslPlugin) actionServlet.getServletContext().getAttribute(
				"fredXslPlugin");
		if (xslPlugin == null) {
			throw new ServletException("Unable to find XslPlugin in servlet context");
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.RequestProcessor#processActionPerform(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.apache.struts.action.Action, org.apache.struts.action.ActionForm, org.apache.struts.action.ActionMapping)
	 */
	protected ActionForward processActionPerform(
		HttpServletRequest request,
		HttpServletResponse response,
		Action action,
		ActionForm form,
		ActionMapping mapping)
		throws IOException, ServletException {

		LogFacility.log("Entering FredRequestProcessor");

		ActionForward forward = null;
		
		// TODO instrument
		// this try/catch is just here to allow the fact.cleanUp() 
		// to get called
		try {
			 forward =
				super.processActionPerform(
					request,
					response,
					action,
					form,
					mapping);
		} catch (ServletException se) {
			throw se;
		} catch (IOException ioe) {
			throw ioe;
		} finally {
			cleanup(request);
		}


		String xslPath = getXslPath(forward);
		if (isXML(xslPath)) {
			LogFacility.log("FredRequestProcessor generating response");
			generateOutput(request, response, form, xslPath);
			forward = null;
		}

		LogFacility.log("FredRequestProcessor done");
		
		return forward;
	}

	private void generateOutput(HttpServletRequest request, HttpServletResponse response, ActionForm form, String xslPath) throws IOException {
		OutputStream ostream = response.getOutputStream();
		FredRenderable renderable = ensureFredRenderable(form);
		
		String xslURI = xslPlugin.getXslUriForPath(xslPath);

		FredRenderer renderer =
			new FredRenderer(xslPlugin.getUriResolver(), ostream);

		renderActionErrors(request, renderer);

		renderer.render(renderable, xslURI);
	}

	private void renderActionErrors(
		HttpServletRequest request,
		FredRenderer renderer) {
		ActionErrors aerrs =
			(ActionErrors) request.getAttribute(Globals.ERROR_KEY);

		MessageResources messages =
			(MessageResources) request.getAttribute(Globals.MESSAGES_KEY);
		if (aerrs != null) {
			renderErrors(renderer, aerrs, messages);
		}

	}

	/**
	 * @param renderer
	 * @param aerrs
	 */
	private void renderErrors(
		FredRenderer renderer,
		ActionErrors aerrs,
		MessageResources messages) {
			
		FredStrutsRenderer myRenderer = new FredStrutsRenderer();
		Iterator errIter = aerrs.get();
		while (errIter.hasNext()) {
			ActionError error = (ActionError) errIter.next();
			Element e = myRenderer.render(error, messages.getMessage(error.getKey(), error.getValues()));
			renderer.addElement(e);
		}
	}

	private String getXslPath(ActionForward forward) {
		String xslPath = "";
		if (forward != null) {
			xslPath = forward.getPath();
		}
		return xslPath;
	}

	private FredRenderable ensureFredRenderable(ActionForm form) {
		if (form instanceof FredRenderable) {
			return (FredRenderable) form;
		} else {
			return new FredRenderableAdapter(form);
		}
	}

	private void cleanup(HttpServletRequest request) {
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.RequestProcessor#destroy()
	 */
	public void destroy() {
		LogFacility.log("FredRequestProcessor destroy");
		super.destroy();
	}
	/* (non-Javadoc)
	 * @see org.apache.struts.action.RequestProcessor#processForwardConfig(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.apache.struts.config.ForwardConfig)
	 */
	protected void processForwardConfig(
		HttpServletRequest request,
		HttpServletResponse response,
		ForwardConfig config)
		throws IOException, ServletException {
		
		if (isXML(config.getPath())) {
			LogFacility.log("Not forwarding "+config);
		} else {
			super.processForwardConfig(request, response, config);
		}
	}

	/**
	 * @param path
	 * @return true if path ends with .xsl
	 */
	private boolean isXML(String path) {
		return path.toLowerCase().endsWith(".xsl");
	}

}

/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/struts/action/ActionFormElement.java,v 1.2 2004/01/02 02:48:58 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2004.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.struts.action;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.dom4j.Element;

import com.cmdev.fred.logging.LogFacility;

/**
 * This class assists in representing a stock Struts
 * ActionForm as an portion of an XML document.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.2 $
 */
public class ActionFormElement {
	
	private ActionForm myForm;
	private Map properties;

	public ActionFormElement(ActionForm aForm) {
		myForm = aForm;
	}

	/**
	 * @param form
	 */
	public Element createClassElement(Element root) {
		return root.addElement(mangleClassName(formClassName(myForm)));
	}

	/**
	 * @param string
	 * @return
	 */
	public String mangleClassName(String className) {
		int lastDot = className.lastIndexOf(".");
		return className.substring(lastDot+1).replace('$', '.');
	}

	public String formClassName(ActionForm aForm) {
		if (aForm == null) {
			return "FredNullForm";
		} else {
			return aForm.getClass().getName();
		}
	}

	/**
	 * @return
	 */
	public void propertyElements(Element root) {
		try {
			properties = BeanUtils.describe(myForm);
			Iterator propertiesIter = properties.keySet().iterator();
			 while (propertiesIter.hasNext()) {
				Object key = (Object) propertiesIter.next();
				if (!skipProperty(key)) {
					Object value = properties.get(key);
					Element e = createElement(key, value, root);
				}
			}
		} catch (Exception e) {
			LogFacility.log(e);
		}
	}

	private Element createElement(Object key, Object value, Element root) {
		LogFacility.log("key=" + key + ", value=" + value);
		Element e = root.addElement(key.toString());
		if (value != null) {
			e.addText(value.toString());
		} else {
			e.addText("null");
		}
		return e;
	}

	/**
	 * @param key
	 * @return
	 */
	private boolean skipProperty(Object key) {
		String propertyName = key.toString();
		return (
				propertyName.equals("class") || 
				propertyName.equals("multipartRequestHandler") || 
				propertyName.equals("servletWrapper"));
	}
	
}

/*
 */
 package com.cmdev.fred.struts.action;

//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.InputStream;
//import java.io.IOException;
//import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

import com.cmdev.fred.logging.LogFacility;

/**
 * A plugin to handle the Fred-style XML/XSL extension it implements 
 * a URIResolver relative to the web application root which Fred uses
 * to find stylesheets for ActionForms.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.2 $ $Date: 2004/01/02 02:49:50 $
 */
public class FredXslPlugin implements PlugIn, URIResolver {

	private String SERVLET_ROOT_PATH;

	private ServletContext servletContext;

	public static final String FRED_XSL_PLUGIN_KEY = "fredXslPlugin";
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.transform.URIResolver#resolve(java.lang.String,
	 *      java.lang.String)
	 */
	public Source resolve(String href, String base)
		throws TransformerException {
		if (href == null) {
			href = "";
		}
		if (base == null) {
			base = "";
		}
		InputStream is = servletContext.getResourceAsStream(base + href);
		StreamSource source = new StreamSource(is);
		return source;
	}

	/**
	 * 
	 */
	public FredXslPlugin() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.PlugIn#destroy()
	 */
	public void destroy() {
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.PlugIn#init(org.apache.struts.action.ActionServlet, org.apache.struts.config.ModuleConfig)
	 */
	public void init(ActionServlet servlet, ModuleConfig config)
		throws ServletException {
			
		servletContext = servlet.getServletContext();
		SERVLET_ROOT_PATH = servletContext.getRealPath("");
		servletContext.setAttribute(FRED_XSL_PLUGIN_KEY, this);
		LogFacility.log("Initializing FredXslPlugin with "+SERVLET_ROOT_PATH);
	}

	/**
	 * @return
	 */
	public URIResolver getUriResolver() {
		return this;
	}

	/**
	 * @param xslPath
	 * @return
	 */
	public String getXslUriForPath(String xslPath) {
		return SERVLET_ROOT_PATH + xslPath;
	}
}

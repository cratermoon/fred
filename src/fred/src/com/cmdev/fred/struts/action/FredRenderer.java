/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/struts/action/FredRenderer.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.struts.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.URIResolver;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import com.cmdev.fred.struts.form.FredRenderable;
import com.cmdev.fred.helper.io.*;
import com.cmdev.fred.helper.transform.*;
import com.cmdev.fred.logging.LogFacility;

/**
 * Renders a FredActionForm from XML with an XSL stylesheet.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $
 */
public class FredRenderer {

	private OutputStream outputStream;
	private URIResolver myUriResolver;
	
	private DocumentFactory factory = DocumentFactory.getInstance();
	private Document xmlDocument;
	private final StyleService styler = new StyleService();
	/**
	 * 
	 */
	public FredRenderer(URIResolver resolver, OutputStream ostream) {
		myUriResolver = resolver;
		outputStream = ostream;
		xmlDocument = factory.createDocument();
		Element root = factory.createElement("fredRenderable");
		xmlDocument.setRootElement(root);
		root.addComment(" Begin Application Data ");
	}

	protected void addElement(Element e) {
		xmlDocument.getRootElement().add(e);
	}
	
	public void render(
		FredRenderable renderable,
		String xslPath) {
		
		LogFacility.log("rendering "+xslPath);

		renderable.renderToDom(xmlDocument);
		TransformerContext ctx = styler.createTransformContext();
		ctx.setXslPath(xslPath);
		ctx.setUriResolver(myUriResolver);
		ctx.setResultOutputStream(outputStream);
		try {
			ctx.setXmlInputStream(new DocumentInputStream(xmlDocument));
			try {
				ctx.setStylesheetInputStream(new FileInputStream(xslPath));
			} catch (FileNotFoundException e1) {
				LogFacility.log(e1);
			}
			styler.transform(ctx);
		} catch (IOException e) {
			LogFacility.log(e);
		}
	}
}

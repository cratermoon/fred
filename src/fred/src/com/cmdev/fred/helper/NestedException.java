/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/helper/NestedException.java,v 1.1 2003/12/30 14:43:21 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.fred.helper;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * An exception that has an optional nested cause Throwable.
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:21 $
 */
public class NestedException extends Exception {

	private Throwable cause;

	/**
	 * 
	 */
	public NestedException() {
		super();
	}

	public NestedException(Throwable t) {
		super();
		cause = t;
	}
	
	public Throwable getCause() {
		return cause;
	}

	/**
	 * @param arg0
	 */
	public NestedException(String message) {
		super(message);
	}

	public NestedException(Throwable t, String message) {
		super(message);
		cause = t;
	}
	/**
	 * Prints a stack trace out for the exception, and
	 * any nested exception that it may have embedded in
	 * its Status object.
	 */
	public void printStackTrace(PrintStream output) {
		super.printStackTrace(output);
		if (cause != null) {
			cause.printStackTrace(output);
		}
	}

	/**
	 * Prints a stack trace out for the exception, and
	 * any nested exception that it may have embedded in
	 * its Status object.
	 */
	public void printStackTrace(PrintWriter output) {
		super.printStackTrace(output);
		if (cause != null) {
			cause.printStackTrace(output);
		}
	}
}

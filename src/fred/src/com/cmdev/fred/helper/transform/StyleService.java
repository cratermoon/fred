/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/helper/transform/StyleService.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2002.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.helper.transform;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import com.cmdev.fred.logging.LogFacility;
/**
 * 
 * 
 * @author     <a href="mailto:sen@cookiehome.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $
 */
public class StyleService {

	private TransformerFactory tf = TransformerFactory.newInstance();
	/**
	 * @return
	 */
	public TransformerContext createTransformContext() {
		return new TransformerContext();
	}

	/**
	 * @param ctx
	 */
	public void transform(TransformerContext ctx) {
		// TODO caching, instrumenting
		long start = System.currentTimeMillis();
		try {
			Transformer transformer = ensureTransformer(ctx);
			StreamSource source = new StreamSource(ctx.getXmlInputStream());
			StreamResult result = new StreamResult(ctx.getResultOutputStream());
			transformer.setURIResolver(ctx.getUriResolver());
			transformer.transform(source, result);
			long time = (System.currentTimeMillis() - start);
			LogFacility.log(ctx.getXslPath() + " transformation, millisecs="+time);
		} catch (TransformerException tfe) {
			LogFacility.log(tfe);
		}
	}

	private Transformer ensureTransformer(TransformerContext ctx) throws TransformerConfigurationException {
		StreamSource styleSource = new StreamSource(ctx.getStylesheetInputStream());
		Transformer transformer;
		try {
			transformer = tf.newTransformer(styleSource);
		} catch (TransformerConfigurationException e) {
			LogFacility.log(e);
			// failed to load the stylesheet, create the null xform
			transformer = tf.newTransformer();
		}
		return transformer;
	}

}

/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/helper/transform/TransformerContext.java,v 1.1 2003/12/30 14:43:20 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2002.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.cmdev.fred.helper.transform;

import java.io.FileInputStream;
import java.io.OutputStream;

import javax.xml.transform.URIResolver;

import com.cmdev.fred.helper.io.DocumentInputStream;

/**
 * 
 * 
 * @author     <a href="mailto:sen@cookiehome.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $
 */
public class TransformerContext {

	private URIResolver myResolver;
	private DocumentInputStream myStream;
	private OutputStream myOutputStream;
	private FileInputStream myStylesheetStream;
	private String myXslPath;
	/**
	 * @param myResolver
	 */
	public void setUriResolver(URIResolver aResolver) {
		myResolver = aResolver;
	}

	public URIResolver getUriResolver() {
		return myResolver;
	}
	/**
	 * @param stream
	 */
	public void setXmlInputStream(DocumentInputStream stream) {
		myStream = stream;
	}

	public DocumentInputStream getXmlInputStream() {
		return myStream;
	}
	
	/**
	 * @param outputStream
	 */
	public void setResultOutputStream(OutputStream outputStream) {
		myOutputStream = outputStream;
	}

	public OutputStream getResultOutputStream() {
		return myOutputStream;
	}
	/**
	 * @param stream
	 */
	public void setStylesheetInputStream(FileInputStream stream) {
		myStylesheetStream = stream;
	}

	public FileInputStream getStylesheetInputStream() {
		return myStylesheetStream;
	}

	/**
	 * @param xslPath
	 */
	public void setXslPath(String xslPath) {
		myXslPath = xslPath;
	}
	
	public String getXslPath() {
		return myXslPath;
	}
}
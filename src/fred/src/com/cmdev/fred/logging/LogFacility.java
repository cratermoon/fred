/*
 * $Header: /opt/cvsroot/Fred/src/com/cmdev/fred/logging/LogFacility.java,v 1.1 2003/12/30 14:43:21 sen Exp $
 * ====================================================================
 * Copyright (c) Crater Moon Development, 2003.  All rights reserved
 * under copyright laws of the United States and other countries.
 * 
 * You may distribute under the terms of either the GNU General Public License
 * or the Artistic License, as specified in the README file.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND^M
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cmdev.fred.logging;

/**
 * Logging interface for Fred.
 * 
 * TODO use log4j
 * 
 * @author     <a href="mailto:sen@cmdev.com">Steven E. Newton</a>
 * @version    $Revision: 1.1 $ $Date: 2003/12/30 14:43:21 $
 */
public class LogFacility {

	/**
	 * 
	 */
	private LogFacility() {
		super();
	}

	public static void log(String message) {
		System.out.println(message);
	}

	public static void log(Throwable t) {
		t.printStackTrace(System.out);
	}

}
